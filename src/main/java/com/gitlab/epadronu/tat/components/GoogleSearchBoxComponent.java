package com.gitlab.epadronu.tat.components;

import com.gitlab.epadronu.tat.core.Component;
import com.gitlab.epadronu.tat.core.Navigable;
import com.gitlab.epadronu.tat.pages.GoogleSearchResultPage;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.Keys.ENTER;

public final class GoogleSearchBoxComponent<P extends Navigable> extends Component<P> {

  public GoogleSearchBoxComponent(final WebElement wrappedElement, final P parent) {
    super(wrappedElement, parent);
  }

  public GoogleSearchResultPage search(final String query) {
    clear();

    sendKeys(query, ENTER);

    return at(GoogleSearchResultPage::new);
  }
}
