package com.gitlab.epadronu.tat.core;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gitlab.epadronu.tat.core.ConfigurationManager.DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS_VALUE;
import static com.gitlab.epadronu.tat.core.ConfigurationManager.Property.DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS;
import static com.gitlab.epadronu.tat.core.ConfigurationManager.getConfigurationValueFor;

public final class Browser implements Navigable {

  private final WebDriver webDriver;

  private final WebDriverWait webDriverWait;

  private Browser(final WebDriver webDriver) {
    this.webDriver = webDriver;
    this.webDriverWait = new WebDriverWait(
     webDriver,
     getConfigurationValueFor(
      DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS,
      DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS_VALUE,
      Long::valueOf));
  }

  public static void browse(
   final Supplier<WebDriver> webDriverFactory,
   final Consumer<Browser> navigableContext) {
    Browser browser = null;

    try {
      browser = new Browser(webDriverFactory.get());

      navigableContext.accept(browser);
    } finally {
      final boolean isSafeToQuitTheDriver = (
       browser != null
       && browser.webDriver != null
       && !browser.webDriver.toString().toLowerCase().contains("null"));

      if (isSafeToQuitTheDriver) {
        browser.webDriver.quit();
      }
    }
  }

  private <T extends Page> boolean isNotAt(final T page) {
    return !page.at().apply(this);
  }

  @Override
  public <T extends Page> T goTo(final Function<Browser, T> pageFactory) {
    final T page = pageFactory.apply(this);

    final String url = page
     .url()
     .orElseThrow(() -> new IllegalArgumentException("The page doesn't have a URL to go to"));

    get(url);

    if (isNotAt(page)) {
      throw new IllegalStateException("The browser is not at a %s page".formatted(page.getClass()));
    }

    return page;
  }

  @Override
  public <T extends Page> T at(final Function<Browser, T> pageFactory) {
    final T page = pageFactory.apply(this);

    if (isNotAt(page)) {
      throw new IllegalStateException("The browser is not at a %s page".formatted(page.getClass()));
    }

    return page;
  }

  @Override
  public void get(final String s) {
    webDriver.get(s);
  }

  @Override
  public String getCurrentUrl() {
    return webDriver.getCurrentUrl();
  }

  @Override
  public String getTitle() {
    return webDriver.getTitle();
  }

  @Override
  public List<WebElement> findElements(final By by) {
    return webDriver.findElements(by);
  }

  @Override
  public WebElement findElement(final By by) {
    return webDriver.findElement(by);
  }

  @Override
  public String getPageSource() {
    return webDriver.getPageSource();
  }

  @Override
  public void close() {
    webDriver.close();
  }

  @Override
  public void quit() {
    webDriver.quit();
  }

  @Override
  public Set<String> getWindowHandles() {
    return webDriver.getWindowHandles();
  }

  @Override
  public String getWindowHandle() {
    return webDriver.getWindowHandle();
  }

  @Override
  public TargetLocator switchTo() {
    return webDriver.switchTo();
  }

  @Override
  public Navigation navigate() {
    return webDriver.navigate();
  }

  @Override
  public Options manage() {
    return webDriver.manage();
  }

  @Override
  public WebDriverWait getWebDriverWait() {
    return webDriverWait;
  }
}
