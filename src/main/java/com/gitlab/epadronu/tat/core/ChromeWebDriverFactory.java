package com.gitlab.epadronu.tat.core;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static java.util.Collections.singletonMap;

final class ChromeWebDriverFactory implements WebDriverFactory {

  private static boolean isTheDriverAlreadySetup = false;

  private boolean disableImagesFlag = false;

  private boolean headlessFlag = false;

  private boolean fullscreenFlag = false;

  private boolean maximizeWindowFlag = false;

  @Override
  public WebDriver build() {
    initialSetup();

    final ChromeOptions options = new ChromeOptions();

    if (disableImagesFlag) {
      options.setExperimentalOption(
       "prefs",
       singletonMap("profile.managed_default_content_settings.images", 2));
    }

    if (headlessFlag) {
      options.addArguments("--headless", "--disable-gpu");
    }

    if (fullscreenFlag) {
      options.addArguments("--kiosk");
    }

    final ChromeDriver driver = new ChromeDriver(options);

    if (maximizeWindowFlag) {
      driver.manage().window().maximize();
    }

    return driver;
  }

  @Override
  public WebDriverFactory disableImages() {
    disableImagesFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory headless() {
    headlessFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory fullscreen() {
    fullscreenFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory maximizeWindow() {
    maximizeWindowFlag = true;

    return this;
  }

  private static void initialSetup() {
    if (!isTheDriverAlreadySetup) {
      WebDriverManager.chromedriver().setup();

      isTheDriverAlreadySetup = true;
    }
  }
}
