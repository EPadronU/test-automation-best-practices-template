package com.gitlab.epadronu.tat.core;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public non-sealed abstract class Component<P extends Navigable> implements Navigable, WebElement {

  private final WebElement wrappedElement;

  private final P parent;

  protected Component(final WebElement wrappedElement, final P parent) {
    this.wrappedElement = wrappedElement;
    this.parent = parent;
  }

  public P getParent() {
    return parent;
  }

  public <T extends Page> T click(final Function<Browser, T> pageFactory) {
    this.click();

    return parent.at(pageFactory);
  }

  @Override
  public <T extends Page> T goTo(final Function<Browser, T> pageFactory) {
    return parent.goTo(pageFactory);
  }

  @Override
  public <T extends Page> T at(final Function<Browser, T> pageFactory) {
    return parent.at(pageFactory);
  }

  @Override
  public void get(final String s) {
    parent.get(s);
  }

  @Override
  public String getCurrentUrl() {
    return parent.getCurrentUrl();
  }

  @Override
  public String getTitle() {
    return parent.getTitle();
  }

  @Override
  public List<WebElement> findElements(final By by) {
    return parent.findElements(by);
  }

  @Override
  public WebElement findElement(final By by) {
    return parent.findElement(by);
  }

  @Override
  public String getPageSource() {
    return parent.getPageSource();
  }

  @Override
  public void close() {
    parent.close();
  }

  @Override
  public void quit() {
    parent.quit();
  }

  @Override
  public Set<String> getWindowHandles() {
    return parent.getWindowHandles();
  }

  @Override
  public String getWindowHandle() {
    return parent.getWindowHandle();
  }

  @Override
  public TargetLocator switchTo() {
    return parent.switchTo();
  }

  @Override
  public Navigation navigate() {
    return parent.navigate();
  }

  @Override
  public Options manage() {
    return parent.manage();
  }

  @Override
  public WebDriverWait getWebDriverWait() {
    return parent.getWebDriverWait();
  }

  @Override
  public void click() {
    wrappedElement.click();
  }

  @Override
  public void submit() {
    wrappedElement.submit();
  }

  @Override
  public void sendKeys(final CharSequence... charSequences) {
    wrappedElement.sendKeys(charSequences);
  }

  @Override
  public void clear() {
    wrappedElement.clear();
  }

  @Override
  public String getTagName() {
    return wrappedElement.getTagName();
  }

  @Override
  public String getAttribute(final String s) {
    return wrappedElement.getAttribute(s);
  }

  @Override
  public boolean isSelected() {
    return wrappedElement.isSelected();
  }

  @Override
  public boolean isEnabled() {
    return wrappedElement.isEnabled();
  }

  @Override
  public String getText() {
    return wrappedElement.getText();
  }

  @Override
  public boolean isDisplayed() {
    return wrappedElement.isDisplayed();
  }

  @Override
  public Point getLocation() {
    return wrappedElement.getLocation();
  }

  @Override
  public Dimension getSize() {
    return wrappedElement.getSize();
  }

  @Override
  public Rectangle getRect() {
    return wrappedElement.getRect();
  }

  @Override
  public String getCssValue(final String s) {
    return wrappedElement.getCssValue(s);
  }

  @Override
  public <X> X getScreenshotAs(final OutputType<X> outputType) throws WebDriverException {
    return wrappedElement.getScreenshotAs(outputType);
  }
}
