package com.gitlab.epadronu.tat.core;

import java.util.function.Function;

public interface ConfigurationManager {

  String DEFAULT_DRIVER_NAME_VALUE = WebDriverFactory.Type.FIREFOX.name();

  long DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS_VALUE = 10L;

  static String getConfigurationValueFor(final Property property, final String defaultValue) {
    return System.getProperty(
     property.value,
     System.getenv().getOrDefault(property.value, defaultValue));
  }

  static <T> T getConfigurationValueFor(
   final Property property,
   final T defaultValue,
   final Function<String, T> mapFunction) {
    return mapFunction.apply(getConfigurationValueFor(property, defaultValue.toString()));
  }

  enum Property {
    DEFAULT_DRIVER_NAME("tat.default.driver.name"),
    DEFAULT_DRIVER_WAIT_TIMEOUT_IN_SECONDS("tat.default.driver.wait.timeout");

    private final String value;

    Property(final String value) {
      this.value = value;
    }
  }
}
