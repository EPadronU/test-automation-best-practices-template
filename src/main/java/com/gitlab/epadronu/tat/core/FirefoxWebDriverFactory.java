package com.gitlab.epadronu.tat.core;

import java.io.IOException;
import java.nio.file.Path;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import static java.nio.file.Files.createTempFile;

final class FirefoxWebDriverFactory implements WebDriverFactory {

  private static final Logger log = LogManager.getLogger();

  private static boolean isTheDriverAlreadySetup = false;

  private boolean disableImagesFlag = false;

  private boolean headlessFlag = false;

  private boolean fullscreenFlag = false;

  private boolean maximizeWindowFlag = false;

  @Override
  public WebDriver build() {
    initialSetup();

    final FirefoxOptions options = new FirefoxOptions();

    if (disableImagesFlag) {
      final FirefoxProfile profile = new FirefoxProfile();

      profile.setPreference("permissions.default.image", 2);

      options.setProfile(profile);
    }

    if (headlessFlag) {
      options.addArguments("--headless");
    }

    final FirefoxDriver driver = new FirefoxDriver(options);

    if (fullscreenFlag) {
      driver.manage().window().fullscreen();
    }

    if (maximizeWindowFlag) {
      driver.manage().window().maximize();
    }

    return driver;
  }

  @Override
  public WebDriverFactory disableImages() {
    disableImagesFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory headless() {
    headlessFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory fullscreen() {
    fullscreenFlag = true;

    return this;
  }

  @Override
  public WebDriverFactory maximizeWindow() {
    maximizeWindowFlag = true;

    return this;
  }

  private static void initialSetup() {
    if (!isTheDriverAlreadySetup) {
      try {
        final Path tempFilePath = createTempFile("firefox-webdriver", "log-dump");

        System.setProperty("webdriver.firefox.logfile", tempFilePath.toAbsolutePath().toString());
      } catch (IOException e) {
        log.error(e);
      }

      WebDriverManager.firefoxdriver().setup();

      isTheDriverAlreadySetup = true;
    }
  }
}
