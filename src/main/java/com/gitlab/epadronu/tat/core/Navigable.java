package com.gitlab.epadronu.tat.core;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

public sealed interface Navigable extends WebDriver, WaitForSupport
 permits Browser, Component, Page {

  Logger log = LogManager.getLogger();

  <T extends Page> T goTo(final Function<Browser, T> pageFactory);

  <T extends Page> T at(final Function<Browser, T> pageFactory);

  default void pause(final long seconds) {
    try {
      TimeUnit.SECONDS.sleep(seconds);
    } catch (InterruptedException e) {
      log.error(e);
    }
  }

  @SuppressWarnings("unchecked")
  default <P extends Navigable, C extends Component<P>> C asComponent(
   final WebElement webElement,
   final BiFunction<? super WebElement, P, C> componentFactory) {
    return componentFactory.apply(webElement, (P) this);
  }

  default <P extends Navigable, C extends Component<P>> C asComponentWaitFor(
   final ExpectedCondition<? extends WebElement> condition,
   final BiFunction<? super WebElement, P, C> componentFactory) {
    return asComponent(waitFor(condition), componentFactory);
  }

  default <P extends Navigable, C extends Component<P>> List<C> asComponentsWaitFor(
   final ExpectedCondition<List<WebElement>> condition,
   final BiFunction<? super WebElement, P, C> componentFactory) {
    return getWebDriverWait()
     .until(condition)
     .stream()
     .map(element -> asComponent(element, componentFactory))
     .collect(Collectors.toList());
  }
}
