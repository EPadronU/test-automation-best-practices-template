package com.gitlab.epadronu.tat.core;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public non-sealed abstract class Page implements Navigable {

  private final Browser browser;

  protected Page(final Browser browser) {
    this.browser = browser;
  }

  protected Optional<String> url() {
    return Optional.empty();
  }

  protected Function<Browser, Boolean> at() {
    return browser -> true;
  }

  @Override
  public <T extends Page> T goTo(final Function<Browser, T> pageFactory) {
    return browser.goTo(pageFactory);
  }

  @Override
  public <T extends Page> T at(final Function<Browser, T> pageFactory) {
    return browser.at(pageFactory);
  }

  @Override
  public void get(final String s) {
    browser.get(s);
  }

  @Override
  public String getCurrentUrl() {
    return browser.getCurrentUrl();
  }

  @Override
  public String getTitle() {
    return browser.getTitle();
  }

  @Override
  public List<WebElement> findElements(final By by) {
    return browser.findElements(by);
  }

  @Override
  public WebElement findElement(final By by) {
    return browser.findElement(by);
  }

  @Override
  public String getPageSource() {
    return browser.getPageSource();
  }

  @Override
  public void close() {
    browser.close();
  }

  @Override
  public void quit() {
    browser.quit();
  }

  @Override
  public Set<String> getWindowHandles() {
    return browser.getWindowHandles();
  }

  @Override
  public String getWindowHandle() {
    return browser.getWindowHandle();
  }

  @Override
  public TargetLocator switchTo() {
    return browser.switchTo();
  }

  @Override
  public Navigation navigate() {
    return browser.navigate();
  }

  @Override
  public Options manage() {
    return browser.manage();
  }

  @Override
  public WebDriverWait getWebDriverWait() {
    return browser.getWebDriverWait();
  }
}
