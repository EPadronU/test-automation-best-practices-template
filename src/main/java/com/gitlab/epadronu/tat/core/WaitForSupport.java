package com.gitlab.epadronu.tat.core;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface WaitForSupport {

  WebDriverWait getWebDriverWait();

  default <T> T waitFor(final ExpectedCondition<T> condition) {
    return getWebDriverWait().until(condition);
  }
}
