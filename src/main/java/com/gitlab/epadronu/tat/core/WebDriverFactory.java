package com.gitlab.epadronu.tat.core;

import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;

import static com.gitlab.epadronu.tat.core.ConfigurationManager.DEFAULT_DRIVER_NAME_VALUE;
import static com.gitlab.epadronu.tat.core.ConfigurationManager.Property.DEFAULT_DRIVER_NAME;
import static com.gitlab.epadronu.tat.core.ConfigurationManager.getConfigurationValueFor;

public sealed interface WebDriverFactory permits ChromeWebDriverFactory, FirefoxWebDriverFactory {

  static WebDriverFactory getInstance() {
    return getInstance(Type.valueOf(
     getConfigurationValueFor(DEFAULT_DRIVER_NAME, DEFAULT_DRIVER_NAME_VALUE).toUpperCase()));
  }

  static WebDriverFactory getInstance(final Type type) {
    return type.factorySupplier.get();
  }

  WebDriver build();

  WebDriverFactory disableImages();

  WebDriverFactory headless();

  WebDriverFactory fullscreen();

  WebDriverFactory maximizeWindow();

  enum Type {
    CHROME(ChromeWebDriverFactory::new),
    FIREFOX(FirefoxWebDriverFactory::new);

    private final Supplier<WebDriverFactory> factorySupplier;

    Type(final Supplier<WebDriverFactory> factorySupplier) {
      this.factorySupplier = factorySupplier;
    }
  }
}
