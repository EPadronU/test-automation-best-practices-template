package com.gitlab.epadronu.tat.pages;

import java.util.Optional;
import java.util.function.Function;

import com.gitlab.epadronu.tat.components.GoogleSearchBoxComponent;
import com.gitlab.epadronu.tat.core.Browser;
import com.gitlab.epadronu.tat.core.Page;
import org.openqa.selenium.By;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

public final class GoogleHomePage extends Page {

  private final By searchBox = By.name("q");

  public GoogleHomePage(final Browser browser) {
    super(browser);
  }

  @Override
  protected Optional<String> url() {
    return Optional.of("https://www.google.com");
  }

  @Override
  protected Function<Browser, Boolean> at() {
    return browser -> waitFor(titleIs("Google"));
  }

  public GoogleSearchBoxComponent<GoogleHomePage> getSearchBox() {
    return asComponentWaitFor(elementToBeClickable(searchBox), GoogleSearchBoxComponent::new);
  }
}
