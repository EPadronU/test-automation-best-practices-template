package com.gitlab.epadronu.tat.pages;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.gitlab.epadronu.tat.components.GoogleSearchBoxComponent;
import com.gitlab.epadronu.tat.core.Browser;
import com.gitlab.epadronu.tat.core.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfElementsToBeMoreThan;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleContains;

public final class GoogleSearchResultPage extends Page {

  private final By searchBox = By.name("q");

  private final By resultsTitle = By.cssSelector("#rso .g h3");

  public GoogleSearchResultPage(final Browser browser) {
    super(browser);
  }

  @Override
  protected Function<Browser, Boolean> at() {
    return browser -> waitFor(titleContains(" - Buscar con Google"));
  }

  public GoogleSearchBoxComponent<GoogleSearchResultPage> getSearchBox() {
    return asComponentWaitFor(elementToBeClickable(searchBox), GoogleSearchBoxComponent::new);
  }

  public List<String> getResultsTitle() {
    return waitFor(numberOfElementsToBeMoreThan(resultsTitle, 0))
     .stream()
     .map(WebElement::getText)
     .collect(Collectors.toList());
  }
}
