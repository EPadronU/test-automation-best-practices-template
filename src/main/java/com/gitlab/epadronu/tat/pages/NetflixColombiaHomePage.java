package com.gitlab.epadronu.tat.pages;

import java.util.Optional;
import java.util.function.Function;

import com.gitlab.epadronu.tat.core.Browser;
import com.gitlab.epadronu.tat.core.Page;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleContains;

public final class NetflixColombiaHomePage extends Page {

  public NetflixColombiaHomePage(final Browser browser) {
    super(browser);
  }

  @Override
  public Optional<String> url() {
    return Optional.of("https://www.netflix.com/co-en");
  }

  @Override
  public Function<Browser, Boolean> at() {
    return browser -> waitFor(titleContains("Netflix Colombia"));
  }
}
