package com.gitlab.epadronu.tat;

import com.gitlab.epadronu.tat.core.Browser;
import com.gitlab.epadronu.tat.core.WebDriverFactory;
import com.gitlab.epadronu.tat.pages.GoogleHomePage;
import org.junit.jupiter.api.Test;

import static com.gitlab.epadronu.tat.core.WebDriverFactory.Type.CHROME;
import static org.assertj.core.api.Assertions.assertThat;

public final class GoogleQueryTest {

  private static final WebDriverFactory webDriverFactory = WebDriverFactory.getInstance(CHROME);

  @Test
  public void lookForLeagueOfLegendsAndCheckFirstResult() {
    Browser.browse(webDriverFactory::build, browser -> {
      final String firstResultTitle = browser
       .goTo(GoogleHomePage::new)
       .getSearchBox()
       .search("LoL")
       .getResultsTitle()
       .stream()
       .findFirst()
       .orElseThrow();

      assertThat(firstResultTitle).isEqualTo("League of Legends");
    });
  }

  @Test
  public void lookForLeagueOfLegendsAndThenLookForDotaAndCheckFirstResult() {
    Browser.browse(webDriverFactory::build, browser -> {
      final String firstResultTitle = browser
       .goTo(GoogleHomePage::new)
       .getSearchBox()
       .search("LoL")
       .getSearchBox()
       .search("Dota")
       .getResultsTitle()
       .stream()
       .findFirst()
       .orElseThrow();

      assertThat(firstResultTitle).isEqualTo("Dota 2");
    });
  }
}
