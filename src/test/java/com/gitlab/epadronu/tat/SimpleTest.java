package com.gitlab.epadronu.tat;

import java.util.function.Supplier;

import com.gitlab.epadronu.tat.core.Browser;
import com.gitlab.epadronu.tat.core.WebDriverFactory;
import com.gitlab.epadronu.tat.pages.NetflixColombiaHomePage;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.Assertions.assertThat;

public final class SimpleTest {

  private static void exerciseTheDriver(final Supplier<WebDriver> webDriverFactory) {
    Browser.browse(webDriverFactory, browser -> {
      final NetflixColombiaHomePage homePage = browser.goTo(NetflixColombiaHomePage::new);

      browser.pause(5L);

      assertThat(homePage.getTitle())
       .isEqualTo("Netflix Colombia - Watch TV Shows Online, Watch Movies Online");
    });
  }

  @Test
  public void openABrowserWithTheDefaultConfiguration() {
    exerciseTheDriver(WebDriverFactory.getInstance()::build);
  }

  @Test
  public void openHeadlessBrowser() {
    exerciseTheDriver(WebDriverFactory.getInstance().headless()::build);
  }

  @Test
  public void openBrowserWithImagesDisabledAndMaximize() {
    exerciseTheDriver(WebDriverFactory.getInstance().disableImages().maximizeWindow()::build);
  }

  @Test
  public void openBrowserInFullscreen() {
    exerciseTheDriver(WebDriverFactory.getInstance().fullscreen()::build);
  }
}
